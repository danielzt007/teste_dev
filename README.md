# teste_dev

#### teste_dev - Projeto de avaliação
- Criado o CRUD de Cliente (id, nome, cpf, dataNascimento)
- Criado API com métodos solicitados (GET, POST, DELETE, PATCH e PUT).
##### Informações Adicionas
- Utilizado Banco de dados SaSS (ElephantSQL).
- Segue arquivo SQL.
- Segue Collection da API (Postman).
- Versão Spring (2.3.2.RELEASE)