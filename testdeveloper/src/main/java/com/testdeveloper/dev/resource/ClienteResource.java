package com.testdeveloper.dev.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.testdeveloper.dev.service.ClienteImplementsService;

import com.testdeveloper.dev.model.Cliente;

@RestController
@RequestMapping("/cliente")
public class ClienteResource {
	
	@Autowired
	private ClienteImplementsService clienteImplementsService;

	public ClienteResource(ClienteImplementsService clienteImplementsService) {
		super();
		this.clienteImplementsService = clienteImplementsService;
	}
	
	@GetMapping
	@ResponseBody
	public ResponseEntity<?> findAll(){
		List<Cliente> clientes = this.clienteImplementsService.mostrarTodos();
		return new ResponseEntity<List>(clientes, HttpStatus.OK);
	}
		
	@GetMapping("/cpf")
	@ResponseBody
	public ResponseEntity<?> pesquisarPorCpf(@Param("cpf") String cpf){
		Cliente cliente = this.clienteImplementsService.pesquisarPorCpf(cpf);
		return new ResponseEntity<Cliente>(cliente, HttpStatus.OK);
	}
	
	@PostMapping
	@ResponseBody
	public Long salvarEmpresa(@RequestBody Cliente cliente) {
		Long clienteX = this.clienteImplementsService.salvarNovoCliente(cliente);
		return clienteX;
	}
	
	@DeleteMapping("/{id}")
	public void deletarEmpresa(@PathVariable("id") Long id) {
		this.clienteImplementsService.excluirCliente(id);
	}	
	
	@PutMapping
	@ResponseBody
	public Cliente atualizarCliente(@RequestBody Cliente cliente) {
		Cliente clienteAtualizado = this.clienteImplementsService.atualizarCliente(cliente); 
		return clienteAtualizado;
	}
	
	@PatchMapping("/{id}/{nome}")
	@ResponseBody
	public Cliente atualizarCliente(@PathVariable("id") Long id, @PathVariable("nome") String nome) {
		Cliente clienteAtualizado = this.clienteImplementsService.atualizarApenasNome(id, nome); 
		return clienteAtualizado;
	} 

}
