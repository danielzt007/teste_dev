package com.testdeveloper.dev.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.testdeveloper.dev.model.Cliente;

public interface ClienteRepository extends JpaRepository<Cliente, Long> {
	
	@Query("select c from Cliente c where c.cpf = ?1")
	public Cliente findByCpf(String cpf);
	
}
