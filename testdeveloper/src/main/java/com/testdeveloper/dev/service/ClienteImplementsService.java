package com.testdeveloper.dev.service;

import java.time.LocalDate;
import java.time.Period;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.testdeveloper.dev.model.Cliente;
import com.testdeveloper.dev.repository.ClienteRepository;

@Service
public class ClienteImplementsService implements ClienteService {

	@Autowired
	private ClienteRepository clienteRepository;
	
	@Override
	public List<Cliente> mostrarTodos() {
		return this.clienteRepository.findAll();
	}

	@Override
	public Cliente pesquisarPorCpf(String cpf) {
		Cliente cliente = this.clienteRepository.findByCpf(cpf);
		return cliente;
	}

	@Override
	public Long salvarNovoCliente(Cliente cliente) {
		Period periodo = Period.between(cliente.getDataNascimento(), LocalDate.now());
		cliente.setIdade(periodo.getYears());
		Cliente clienteNovo = this.clienteRepository.save(cliente);
		return clienteNovo.getId();
	}

	@Override
	public void excluirCliente(Long id) {
		Cliente cliente = this.clienteRepository.getOne(id); 
		this.clienteRepository.deleteById(cliente.getId());
	}

	@Override
	public Cliente atualizarCliente(Cliente cliente) {
		Cliente clienteAtualizado = this.clienteRepository.save(cliente);
		return clienteAtualizado;
	}

	@Override
	public Cliente atualizarApenasNome(Long id, String nome) {
		Optional<Cliente> cliente = this.clienteRepository.findById(id);
		Cliente clienteX = cliente.get();
		if(clienteX != null) {
			clienteX.setNome(nome);
			this.clienteRepository.save(clienteX);
		}
		return clienteX;
	}

}
