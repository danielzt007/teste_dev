package com.testdeveloper.dev.service;

import java.util.List;

import com.testdeveloper.dev.model.Cliente;

public interface ClienteService {
	
	public List<Cliente> mostrarTodos();
	public Cliente pesquisarPorCpf(String cpf);
	public Long salvarNovoCliente(Cliente cliente);
	public void excluirCliente(Long id);
	public Cliente atualizarCliente(Cliente cliente);
	public Cliente atualizarApenasNome(Long id, String nome);

}
